-- function conky_pad( number )
--     return string.format( "%3i" , conky_parse( number ) )
-- end

function conky_ptu(number)
    local p = tonumber(conky_parse('${cpu cpu' .. number .. '}'))

    if p <= 5 then
        return " "
    elseif p <= 18 then
        return "<span color='\\#3fff00'>▁</span>"
    elseif p <= 31 then
        return "<span color='\\#7fff00'>▂</span>"
    elseif p <= 44 then
        return "<span color='\\#bfff00'>▃</span>"
    elseif p <= 56 then
        return "<span color='\\#ffff00'>▄</span>"
    elseif p <= 69 then
        return "<span color='\\#ffbf00'>▅</span>"
    elseif p <= 82 then
        return "<span color='\\#ff7f00'>▆</span>"
    elseif p <= 95 then
        return "<span color='\\#ff3f00'>▇</span>"
    else
        return "<span color='\\#ff0000'>█</span>"
    end
end

function conky_cpu_all(number)
  number = tonumber(number)
  local ret = ""
  for i = 1, number do
    ret = ret .. conky_ptu(i)
    if i < number then ret = ret .. " " end
  end

  return ret
end

function cover_length(x1, x2, y1, y2)
  if x2 < y1 or x1 > y2 then
    return nil
  else
    return math.min(x2, y2) - math.max(x1, y1)
  end
end

function get_number(field)
  local s = conky_parse('${' .. field .. '}')
  local s2 = string.gsub(s, '[^0-9%.]', "")

  local mult = 1
  if     string.match(s, '.*K') then mult = 1000
  elseif string.match(s, '.*M') then mult = 1000000
  elseif string.match(s, '.*G') then mult = 1000000000
  end

  return tonumber(s2) * mult
end

function conky_membar_utf(width)
  local colours = {"009f00", "9f9f00", "202020"}
  local muse = get_number("mem")
  local mbuf = get_number("memwithbuffers")
  local mtot = get_number("memmax")
  do
    if mtot == 0 then return "Unk" end
  end
  local muse_k = muse * width / mtot
  local mbuf_k = mbuf * width / mtot
  local ret = ""

  for i = 0, width - 1 do
    local kl = {
      cover_length(0, muse_k, i, i + 1),
      cover_length(muse_k, mbuf_k, i, i + 1),
      cover_length(mbuf_k, width - 1, i, i + 1)
    }
--ret = ret .. tostring(muse) .. "/" .. tostring(mbuf) .. "/" .. tostring(mtot) .. " "
--ret = ret .. tostring(muse_k) .. "/" .. tostring(mbuf_k) .. "/" .. tostring(width) .. " "
--ret = ret .. tostring(kl[1]) .. '/' .. tostring(kl[2]) .. "/" .. tostring(kl[3]) .. " "
    local nk = 0
    for i, v in pairs(kl) do
      if v then nk = nk + 1 end
    end

    if nk == 3 then
      local mi = 1
      if kl[2] < kl[mi] then mi = 2 end
      if kl[3] < kl[mi] then mi = 3 end
      kl[mi] = nil
      nk = 2
      local sum = 0
      for i, v in pairs(kl) do
        if v then sum = sum + v end
      end
      for i, v in pairs(kl) do
        if v then kl[i] = kl[i] / sum end
      end
    end

    if nk == 1 then
      ret = ret .. "<span color='\\#"
      if kl[1] then
        ret = ret .. colours[1]
      elseif kl[2] then
        ret = ret .. colours[2]
      else
        ret = ret .. colours[3]
      end
      ret = ret .. "'>█</span>"
    elseif nk == 2 then
      local fg, bg, size = nil, nil, nil
      if kl[1] then
        fg = colours[1]
        bg = colours[2]
        size = math.floor(kl[1] * 8)
      else
        fg = colours[2]
        bg = colours[3]
        size = math.floor(kl[2] * 8)
      end
      local c = string.char(226, 150, 143 - size)
      ret = ret .. "<span color='\\#" .. fg
                .. "' bgcolor='\\#" .. bg
                .. "'>" .. c .. "</span>"
    else
      ret = ret .. "E"
    end
  end

  return ret
end

function percent_to_colour(p)
    if p <= 5 then
        return "<span color='\\#ff0000'>"
    elseif p <= 18 then
        return "<span color='\\#ff3f00'>"
    elseif p <= 31 then
        return "<span color='\\#ff7f00'>"
    elseif p <= 44 then
        return "<span color='\\#ffbf00'>"
    elseif p <= 56 then
        return "<span color='\\#ffff00'>"
    elseif p <= 69 then
        return "<span color='\\#bfff00'>"
    elseif p <= 82 then
        return "<span color='\\#7fff00'>"
    elseif p <= 95 then
        return "<span color='\\#3fff00'>"
    else
        return "<span color='\\#00ff00'>"
    end
end

function battery_present(s, pc, time)
    local alarm = pc < 10
    local ret = "<span color='\\#"

    if s == "E" or s == "D" then
      ret = ret .. (alarm and "ff0000" or "ffff00")
                .. "'>🔋"
    else
      ret = ret .. (alarm and "ff0000" or "00ff00")
                .. "'>🔌"
    end

    ret = ret .. "</span> "
              .. percent_to_colour(pc)
              .. tostring(pc) .. "%"
              .. "</span> "

    if s == "D" and pc < 10 or s == "E" then
        ret = ret .. "<span color='\\#ff0000'>"
                  .. time
                  .. "</span>"
    else
        ret = ret .. time
    end

    return ret
end

function conky_wifi_perc(dev)
    local p = conky_parse('${wireless_link_qual_perc ' .. dev .. '}')
    local i = math.floor(p * 8 / 100)
    local c = string.char(226, 150, 129 + i)
    return percent_to_colour(tonumber(p)) .. c .. '</span>'
end

function conky_battery_all()
    local bs = conky_parse('${battery_short}')
    local pc = tonumber(conky_parse('${battery_percent}'))
    local time = conky_parse('${battery_time}')

    bs = string.sub(bs, 1, 1)

    if string.find(bs, "[CDEF]") then
        return battery_present(bs, pc, time)
    elseif bs == "N" then
        return "No bat."
    elseif bs == "U" then
        return "Bat.?"
    else
        return "Err." .. bs
    end
end
